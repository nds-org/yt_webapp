import os
from kombu import Queue
from datetime import timedelta
import json
import etcd

# Broker settings.
host_ip = os.environ.get('COREOS_PRIVATE_IPV4', None)
if host_ip is None:
    BROKER_URL = os.getenv('BROKER_URL', 'amqp://guest:guest@localhost:5672')
else:
    client = etcd.Client(host=host_ip, port=4001)
    rserv = json.loads(client.get('/rabbitmq/service').value)
    passw = client.get('/rabbitmq/users/ytfido').value
    BROKER_URL = 'amqp://ytfido:%s@%s:%s/ythub' % \
        (passw, rserv["host"], rserv["port"])
    del client, rserv

BROKER_HEARTBEAT = 0
# BROKER_HOST = "localhost"
# BROKER_PORT = 27017
# BROKER_TRANSPORT = 'mongodb'
# BROKER_VHOST = 'celery'

BROKER_POOL_LIMIT = 500

CELERY_ACKS_LATE = False
CELERY_DISABLE_RATE_LIMITS = True
CELERY_EVENT_SERIALIZER = 'json'
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_IMPORTS = ('backend.v1',)
CELERY_RESULT_BACKEND = "amqp"
CELERY_DEFAULT_QUEUE = 'payment_default1'
CELERY_QUEUES = (
    Queue('default', routing_key='default'),
    Queue('payment_default1', routing_key='payment.tasks1'),
    Queue('payment_default2', routing_key='payment.tasks1'),
)
CELERY_DEFAULT_EXCHANGE = 'payment.tasks1'
CELERY_DEFAULT_EXCHANGE_TYPE = 'direct'
CELERY_DEFAULT_ROUTING_KEY = 'payment.tasks1'

# CELERY_IGNORE_RESULT = True  # useful for fire and forget mode

CELERYD_PREFETCH_MULTIPLIER = 0
CELERYD_MAX_TASKS_PER_CHILD = 100

CELERYBEAT_SCHEDULE = {
    'cull-containers-every-2-hours': {
        'task': 'backend.v1.tasks.cull_containers',
        'schedule': timedelta(hours=2),
    },
}

CELERY_TIMEZONE = 'UTC'
