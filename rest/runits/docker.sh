#!/bin/sh
#set -e
if [ -f /var/run/docker.pid ] ; then
   echo "Everything's fine! carry on!"
   sleep 60
else
   echo "Starting wrapper"
   exec /usr/local/bin/wrapdocker
fi
