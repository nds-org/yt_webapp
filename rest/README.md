yt Hub Web interface
====================

This is a Python web app that manages the creation of containers, and redirects
browsers to use the services running on those containers by presenting links to
the exposed container ports.


Installation
------------

Run this once:

```
#!bash
$ docker pull xarthisius/yt_webapp:rest
```

To run:


```
#!bash
$ docker run --name rmq -d -p 5672:5672 -p 15672:15672 dockerfile/rabbitmq #Start up a RabbitMQ container
$ docker run --privileged --link rmq:rabbitmq -h yt_serve -e "port5888=8581" -e "hostsname=localhost" -p 8523:22 -p 8581:5888 xarthisius/yt_webapp:rest
```

Please be aware that it may take awhile before webapp is fully operational as it downloads quite a big docker images (ytproject/yt-devel:hublaunch)
