import docker
import os
import json
import threading
import datetime
import time
from celery import Celery

AMPQ_IP = os.environ.get('RABBITMQ_PORT_5672_TCP_ADDR', '127.0.0.1')
AMPQ_PORT = os.environ.get('RABBITMQ_PORT_5672_TCP_PORT', '5672')

AMPQ_URI = "amqp://{0}:{1}".format(AMPQ_IP, AMPQ_PORT)

celery = Celery("tasks", broker=AMPQ_URI)
celery.conf.CELERY_RESULT_BACKEND = os.environ.get(
    'CELERY_RESULT_BACKEND', 'amqp')

#Only allow JSON serialisation, disallow pickle.
celery.conf.update(CELERY_ACCEPT_CONTENT = ['json'])

CONTAINER_STORAGE = "/tmp/containers.json"
__UPLOADS__ = "uploads/"
BASE_IMAGE = 'ytproject/yt-devel'
BASE_IMAGE_TAG = 'hublaunch'
CONTAINER_STORAGE = "/tmp/containers.json"
name = "xarthisius"
workdir = "results"
lock = threading.Lock()


class ContainerException(Exception):

    """
    There was some problem generating or launching a docker container
    for the user
    """
    pass


def get_image(image_name, image_tag):
    # TODO catch ConnectionError - requests.exceptions.ConnectionError
    for image in dcli.images():
        if image['Repository'] == image_name and image['Tag'] == image_tag:
            return image
    raise ContainerException("No image found")
    return None


def remember_container(name, containerid):
    container_store = CONTAINER_STORAGE
    with lock:
        if not os.path.exists(container_store):
            containers = {}
        else:
            containers = json.load(open(container_store, 'rb'))
        try:
            containers[name].append(containerid)
        except KeyError:
            containers[name] = [containerid]
        json.dump(containers, open(container_store, 'wb'))


@celery.task
def create_job_docker(script, image_name=BASE_IMAGE, image_tag=BASE_IMAGE_TAG):
    bind_src, script_file = os.path.split(script)
    image = get_image(image_name, image_tag)
    mountpoint = '/blah'
    cont = dcli.create_container(
        image['Id'],
        ['python', '-u', os.path.join(mountpoint, script_file)],
        hostname="{user}box".format(user=name.split('-')[0]),
        ports=[8888, 4200],
        working_dir=workdir,
        volumes=[mountpoint]
    )
    remember_container(name, cont.get('Id'))
    # check_memory()
    dcli.start(cont['Id'],
               binds={bind_src: {"bind": mountpoint}},
               publish_all_ports=True)
    return {'Id': cont['Id'], 'script': script_file}


@celery.task
def get_job_state_docker(cont_id):
    try:
        info = dcli.inspect_container(cont_id)
    except docker.errors.APIError, e:
        return {"State": "UNKNOWN", "error": e.explanation}
    state = info['State']
    # {u'Pid': 0, u'Paused': False, u'Running': False, u'FinishedAt': u'2014-10-13T18:13:18.199756378Z',
    # u'Restarting': False, u'StartedAt': u'2014-10-13T17:13:15.703794407Z',
    # u'ExitCode': 0}
    if state["Running"]:
        return {"State": "RUNNING", "StartedAt": state["StartedAt"]}
    if state['ExitCode'] != 0:
        return {"State": "FAILED", "ExitCode": state['ExitCode']}
    return {"State": "COMPLETED"}


@celery.task
def get_job_stdout_docker(cont_id):
    try:
        logs = dcli.logs(cont_id, stdout=True, stderr=True,
                         stream=False, timestamps=False)
    except docker.errors.APIError, e:
        return {"stdout": None, "error": e.explanation}
    return {"stdout": logs}


@celery.task
def get_job_metric_docker(cont_id):
    try:
        data = dcli.inspect_container(cont_id)
    except docker.errors.APIError, e:
        return {"error": e.explanation}
    return data

if __name__ == "__main__":
    dcli = docker.Client(base_url='unix://var/run/docker.sock',
                         version='1.6',
                         timeout=10)
    dcli.pull(BASE_IMAGE, tag=BASE_IMAGE_TAG)
    celery.start()
